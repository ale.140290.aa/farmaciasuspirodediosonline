window.addEventListener('load', function(){

    txtcedula.addEventListener('blur',function(){
        mensajecedula.innerHTML=""
        if (txtcedula.value.length!=10)
        {
            mensajecedula.innerHTML+="el numero de caracteres debe ser 10 <br>"
        }
        if (!validarcedula(txtcedula.value))
        {
            alert('No cumple los criterios de validacion de Registro Civil'); 
        }
    })
    
    txtemail.addEventListener('blur',function(){
        if(! validateEmail(txtemail.value)) 
        { 
            alert('Debes escribir un email válido'); 
        }
    })

    txtcuenta.addEventListener('blur',function(){
        if(! validateEmail(txtcuenta.value)) 
        { 
            alert('Debes escribir un email válido'); 
        }
    })

    
})

// ------------------------------------------ F U N C I O N E S >^-^< ------------------------------------------

// ------------------------------------------ C E D U L A >^-^< ------------------------------------------

function validarcedula(parametro)
{
    if (parametro.length!=10) return false;

    let semaforo=false
    calculo=0;
    contador=0;

    parametro.substr(0,9).split('').forEach(element => {
        let numeroActual = semaforo ? parseInt(element): parseInt(element)*2;
        calculo+= numeroActual>9 ? numeroActual-9: numeroActual;
        semaforo= !semaforo;
        
    })

    while (calculo>0) calculo-=10;
    return parseInt(parametro.substr(9)) + calculo ==0;
}

// ------------------------------------------ E M A I L >^-^< ------------------------------------------

function validateEmail(email) {
    var re =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

// ------------------------------------------ C O N T R A S E Ñ A >^-^< ------------------------------------------


function comprobar() {
    var p1 = document.formulario.txtpass.value;
    var p2 = document.formulario.txtconfirpass.value;
    
    if (p1 != p2) {
       alert("Las pass no coinciden");
    } else {
        alert("");
    }
 }

// ------------------------------------------ N I C K >^-^< ------------------------------------------

function validaSoloTexto(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toString();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyzÁÉÍÓÚABCDEFGHIJKLMNÑOPQRSTUVWXYZ";//Se define todo el abecedario que se quiere que se muestre.
        especiales = [8, 37, 39, 46, 6]; //Es la validación del KeyCodes, que teclas recibe el campo de texto.
    
        tecla_especial = false
        for(var i in especiales) {
            if(key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }
    
        if(letras.indexOf(tecla) == -1 && !tecla_especial){
            alert('Tecla no aceptada');
            return false;
        }
}

// ------------------------------------------ N O M B R E >^-^< ------------------------------------------

function letrasNumeros(string){//solo letras y numeros
    var out = '';
	//Se añaden las letras validas
    var filtro = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890';//Caracteres validos
	
    for (var i=0; i<string.length; i++)
       if (filtro.indexOf(string.charAt(i)) != -1) 
		   out += string.charAt(i);
    return out;
}

// ------------------------------------------ N U M E R O S >^-^< ------------------------------------------

$(function(){
    $(".validar").keydown(function(event){
        //alert(event.keyCode);
        if((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105) && event.keyCode !==190  && event.keyCode !==110 && event.keyCode !==8 && event.keyCode !==9  ){
            return false;
        }
    });
});