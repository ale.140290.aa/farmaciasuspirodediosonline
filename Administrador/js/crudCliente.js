window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtcedula.value="";
        txtuser.value="";
        txtemail.value="";
        txtfecha.value="";
        txtpass.value="";
        txtconfirpass.value="";
        txtpro.value="";
        txtciu.value="";
        txtdir.value="";
        txtadd.value="";
        txttelf.value="";
        txttelfcon.value="";
    })

    //URL Firebase
    //https://farmaciasuspirodedios.firebaseio.com/cliente.json

    btnconsultar.addEventListener('click',function(){
        //utilizando el metodo GET - Consulta
        let url = `https://farmaciasuspirodedios.firebaseio.com/cliente.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{

            //console.log( Object.entries(resultado2) )

            let tablaHtml= "<table style='width: 100%;  border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>";
            for ( let elemento in resultado2 )
            {
                tablaHtml+="<tr style='background-color: #FBFBEF; text-align: center;'>"

                tablaHtml+=` <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>
                            <button class='boton' style=' width: 150px; height: 30px; font-size: 14px; font-weight: bold; color: rgb(13, 73, 177); 
                            background-image: -moz-linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);background-image: linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);
                            border-radius: 10px; border: 1px solid #66add6;cursor: pointer;'>  
                            ${resultado2[elemento].cedula } 
                            </button>   
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'> 
                            ${resultado2[elemento].nombreApellido}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].email}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'> 
                            ${resultado2[elemento].fecha}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].contraseña}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>
                            ${resultado2[elemento].confContra}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].provincia}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].ciudad}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].direccion}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].adicional}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].celular}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>
                            ${resultado2[elemento].convencional}  
                            </td>`

                tablaHtml+="</tr>"
            }

            tablaHtml+="</table>"

            divconsulta.innerHTML= tablaHtml;

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url2 = `https://farmaciasuspirodedios.firebaseio.com/cliente/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(respuesta=>{return respuesta.json()}).then(respuesta2=>{
                    
                        txtcedula.value=respuesta2.cedula;
                        txtuser.value=respuesta2.nombreApellido;
                        txtemail.value=respuesta2.email;
                        txtfecha.value=respuesta2.fecha;
                        txtpass.value=respuesta2.contraseña;
                        txtconfirpass.value=respuesta2.confContra;
                        txtpro.value=respuesta2.provincia;
                        txtciu.value=respuesta2.ciudad;
                        txtdirc.value=respuesta2.direccion;
                        txtadd.value=respuesta2.adicional;
                        txttelf.value=respuesta2.celular;
                        txttelfcon.value=respuesta2.convencional;
                    } )

                    
                })
            })



        })
        .catch(error=>{
            console.log(error)
        })


        
    })

    btngrabar2.addEventListener('click',function(){
        event.preventDefault();
        let url = `https://farmaciasuspirodedios.firebaseio.com/cliente/${txtcedula.value}.json`
        let cuerpo = {  cedula:txtcedula.value,
                        nombreApellido:txtuser.value,
                        email: txtemail.value,
                        fecha:txtfecha.value,
                        contraseña:txtpass.value,
                        confContra:txtconfirpass.value,
                        provincia:txtpro.value,
                        ciudad:txtciu.value,
                        direccion:txtdir.value,
                        adicional:txtadd.value,
                        celular:txttelf.value,
                        convencional:txttelfcon.value 
            
        }

        fetch(url , {
            method:'PUT',
            body:  JSON.stringify(cuerpo) ,
            headers:{
                'Content-Type':'application/json'
            }

        } )
        .then(respuesta=>{
            return respuesta.json()
        })
        .then(respuesta2=>{
            console.log(respuesta2)
        })
        .catch(error=>{
            console.error('No se pudo grabar el nodo cliente',error);
        })

    })



    btneliminar.addEventListener('click',function(){
        let url = `https://farmaciasuspirodedios.firebaseio.com/cliente/${txtcedula.value}.json`

        fetch(url , {
            method:'DELETE'
        } )
        .then(resultado=>{
            return resultado.json()
        })
        .then(resultado2=>{
            console.log(resultado2)
        })
        .catch(error=>{
            console.error('No se pudo eliminar el cliente',error)
        })


    })
})