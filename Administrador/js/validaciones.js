document.addEventListener("DOMContentLoaded", function(event) {
    document.getElementById('mi_formulario').addEventListener('submit', manejadorValidacion)
});
  
function manejadorValidacion(e) {
    e.preventDefault();
    if(this.querySelector('[name=txtuser]').value == '') 
    {
        console.log('El nombre está vacío');
        alert('Debes escribir un nombre');
        return;
    }
    if(this.querySelector('[name=txtpass]').value == '') 
    {
        console.log('La  clave está vacía');
        alert('Debes escribir una contraseña');
        return;
    }
       if(niveldificultad == null || niveldificultad == 0){
        console.log('ERROR: Debe seleccionar una opcion del combo box');
        alert('ERROR: Debe seleccionar una opcion del combo box');
        return false;
      }
    this.submit();
}
  
