window.addEventListener('load',function(){
    btnnuevo.addEventListener('click',function(){
        txtcode.value="";
        txtname.value="";
        txtpresentacion.value="";
        txtdescp.value="";
        txtmedida.value="";
        txtpre.value="";
        txtpro.value="";
        txtestado.value="";
        txtmarca.value="";
        txtimg.value="";
    })

    //URL Firebase
    //https://farmaciasuspirodedios.firebaseio.com/producto.json

    btnconsultar.addEventListener('click',function(){
        //utilizando el metodo GET - Consulta
        let url = `https://farmaciasuspirodedios.firebaseio.com/producto.json`
        fetch(url).then(resultado=>{
            return resultado.json();
        })
        .then(resultado2=>{

            //console.log( Object.entries(resultado2) )

            let tablaHtml= "<table style='width: 100%;  border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>";
            for ( let elemento in resultado2 )

           
            {
                tablaHtml+="<tr style='background-color: #FBFBEF; text-align: center;'>"

                tablaHtml+=`<td style='width: 100px; height: 35px; border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9; '> 

                            <button class='boton' style=' width: 80px; height: 30px; font-size: 14px; font-weight: bold; color: rgb(13, 73, 177); 
                            background-image: -moz-linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);background-image: linear-gradient(top left 90deg, #acd6ef 0%, #6ec2e8 100%);
                            border-radius: 10px; border: 1px solid #66add6;cursor: pointer;'> 
                            ${resultado2[elemento].codigo } 
                            </button>   
                            </td> 

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].producto}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].presentacion}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].descripcion}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].uniMedida}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9; '>  
                            ${resultado2[elemento].preVenta}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].cantidad}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9;'>  
                            ${resultado2[elemento].estado}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9; '>  
                            ${resultado2[elemento].marca}  
                            </td>

                            <td style='width: 100px; height: 35px;border: 1px solid #a8c9e4; box-shadow: inset 0 1.5px 3px rgba(190, 190, 190, .4), 0 0 0 5px #e6f2f9; '>  
                            ${resultado2[elemento].imagen}  
                            </td>`

                tablaHtml+="</tr>"
            }

            tablaHtml+="</table>"

            divconsulta.innerHTML= tablaHtml;

            document.querySelectorAll('.boton').forEach(elemento=>{
                elemento.addEventListener('click',function(){
                    let url2 = `https://farmaciasuspirodedios.firebaseio.com/producto/${elemento.innerHTML.trim()}.json`
                    console.log(url2)

                    fetch(url2).then(respuesta=>{return respuesta.json()}).then(respuesta2=>{
                    
                        txtcode.value=respuesta2.codigo;
                        txtname.value=respuesta2.producto;
                        txtpresentacion.value=respuesta2.presentacion;
                        txtdescp.value=respuesta2.descripcion;
                        txtmedida.value=respuesta2.uniMedida;
                        txtpre.value=respuesta2.preVenta;
                        txtpro.value=respuesta2.cantidad;
                        txtestado.value=respuesta2.estado;
                        txtmarca.value=respuesta2.marca;
                        txtimg.value=respuesta2.imagen;
                    } )                    
                })
            })
        })
        .catch(error=>{
            console.log(error)
        })


        
    })

    btngrabar2.addEventListener('click',function(){
        event.preventDefault();
        let url = `https://farmaciasuspirodedios.firebaseio.com/producto/${txtcode.value}.json`
        let cuerpo = {  codigo:txtcode.value,
                        producto:txtname.value,
                        presentacion:txtpresentacion.value,
                        descripcion:txtdescp.value,
                        uniMedida:txtmedida.value,
                        preVenta:txtpre.value,
                        cantidad:txtpro.value,
                        estado:txtestado.value,
                        marca:txtmarca.value,
                        imagen:txtimg.value            
        }

        fetch(url , {
            method:'PUT',
            body:  JSON.stringify(cuerpo) ,
            headers:{
                'Content-Type':'application/json'
            }

        } )
        .then(respuesta=>{
            return respuesta.json()
        })
        .then(respuesta2=>{
            console.log(respuesta2)
        })
        .catch(error=>{
            console.error('No se pudo grabar el nodo producto',error);
        })

    })



    btneliminar.addEventListener('click',function(){
        let url = `https://farmaciasuspirodedios.firebaseio.com/producto/${txtcode.value}.json`

        fetch(url , {
            method:'DELETE'
        } )
        .then(resultado=>{
            return resultado.json()
        })
        .then(resultado2=>{
            console.log(resultado2)
        })
        .catch(error=>{
            console.error('No se pudo eliminar el producto',error)
        })


    })
})